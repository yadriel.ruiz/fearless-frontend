function createCard(title, description, pictureUrl, startsDate, endsDate, location,) {
    return `
    <div class="col-4">
        <div class="card shadow mb-4">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${title}</h5>
                <p class="card-subtitle mb-2 text-body-tertiary">${location}</p>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer text-body-secondary">
            <p> ${startsDate.toDateString()} - ${endsDate.toDateString()}</p>
            </div>
        </div>
    </div>
    `;
    }


window.addEventListener('DOMContentLoaded', async() => {
    const url = 'http://localhost:8000/api/conferences/';
    
    try {
    const response = await fetch(url);

    if (!response.ok) {
      // Create new error object and populates in the html the error message
        const message = `An error has occured: ${response.status}`;
        throw new Error(message);


        

    } else {
        const data = await response.json();
        for (let conference of data.conferences) {

        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        

        if (detailResponse.ok) {
            const details = await detailResponse.json();

            // finds the title name and description for the conference
            const title = details.conference.name;
            const description = details.conference.description;

            // finds the picture url for the conference
            const pictureUrl = details.conference.location.picture_url;

            // finds the start and end date for the conference
            const startsDate = new Date(details.conference.starts);
            const endsDate = new Date(details.conference.ends);

            // finds the location for the conference
            const location = details.conference.location.name;

            // creates the card with the information
            const html = createCard(title, description, pictureUrl, startsDate, endsDate,location);
            const row = document.querySelector('.row');
            row.innerHTML += html;
        }
    }
    }
    } catch (e) {
        console.error(e);
    // Says what to do when the error is catched
    const row = document.querySelector('.row');
    row.innerHTML = `<div class="col-12">
    <div class="alert alert-danger" role="alert">
        ${e.message}
    </div>
    </div>`;
    }
});