// event listener for when the page loads
window.addEventListener('DOMContentLoaded', async() => {
    // get the current URL
    const url = "http://localhost:8000/api/states/";
    // fetch the URL awaiting the response
    const response = await fetch(url);
    // if the response is not ok, throw an error
    if (!response.ok) {
        const message = `An error has occured: ${response.status}`;
        throw new Error(message);
    } else {
        // if the response is ok, get the data from the response
        const data = await response.json();

        // for each state in the data, get the name and the abbreviation
        for (let state of data.states) {
            const name = state.name;
            const abbreviation = state.abbreviation;

            // create an option element with the name and abbreviation
            const option = document.createElement('option');
            option.value = abbreviation;
            option.textContent = name;
            
            // append the option to the select element
            const select = document.querySelector('select');
            select.appendChild(option);
        }
    }

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
        formTag.reset();
        const newLocation = await response.json();
        }
    });
});
