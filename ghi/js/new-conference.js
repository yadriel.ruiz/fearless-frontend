// event listener for when the page loads
window.addEventListener('DOMContentLoaded', async() => {
    // get the current URL
    const url = "http://localhost:8000/api/locations/";
    // fetch the URL awaiting the response
    const response = await fetch(url);
    // if the response is not ok, throw an error
    if (!response.ok) {
        const message = `An error has occured: ${response.status}`;
        throw new Error(message);
    } else {
        // if the response is ok, get the data from the response
        const data = await response.json();

        // for each location in the data, get the name and the abbreviation
        for (let location of data.locations) {
            const id = location.id;
            const name = location.name;

            // create an option element with the name and abbreviation
            const option = document.createElement('option');
            option.value = id;
            option.textContent = name;
            
            // append the option to the select element
            const select = document.querySelector('select');
            select.appendChild(option);
        }
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
        formTag.reset();
        const newConference = await response.json();
        }
    });
});
