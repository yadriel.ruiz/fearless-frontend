import React, {useEffect, useState} from 'react';

function ConferenceForm(){
    const [locations, setLocation] = useState([]);

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [starts, setStarts] = useState('');
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }

    const [ends, setEnds] = useState('');
    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }

    const [description, setDescription] = useState('');
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const [presentations, setPresentations] = useState('');
    const handlePresentationsChange = (event) => {
        const value = event.target.value;
        setPresentations(value);
    }


    const [maximumAttendees, setMaximumAttendees] = useState('');
    const handleMaximumAttendeesChange = (event) => {
        const value = event.target.value;
        setMaximumAttendees(value);
    }

    const [location, setLocations] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocations(value);
    }





    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
    
        const response = await fetch(url);
    
        if (response.ok) {
            const data = await response.json();
            setLocation(data.locations);
            }
        }
    
        useEffect(() => {
            fetchData();
        }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = presentations;
        data.max_attendees = maximumAttendees;
        data.location = location;

        console.log(data);

        const url = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setPresentations('');
            setMaximumAttendees('');
            setLocations('');
        }
        const formTag = document.getElementById('create-conference-form');
        formTag.reset();
    }

        


    return (   
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleStartsChange} type="date" className="form-control" id="starts" name="starts"/>
                        <label htmlFor="starts">Starts</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleEndsChange} type="date" className="form-control" id="ends" name="ends"/>
                        <label htmlFor="ends">Ends</label>
                    </div>

                    <div className="mb-3">
                        <label htmlFor="description" className="form-label">Description</label>
                        <textarea onChange={handleDescriptionChange} className="form-control" id="description" rows="3" name="description"></textarea>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handlePresentationsChange} className="form-control" required type="number"  id="presentations" name="max_presentations"/>
                        <label htmlFor="presentations">Maximum Presentations</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleMaximumAttendeesChange} className="form-control" required type="number" id="attendees" name="max_attendees"/>
                        <label htmlFor="attendees">Maximum Attendees</label>
                    </div>

                <div className="mb-3">
                    <select onChange={handleLocationChange} required id="location" className="form-select" name="location">
                    <option value="">Choose a Location</option>
                {locations.map(location => {
                    return (
                        <option key={location.name} value={location.id}>
                            {location.name}
                        </option>
                    );
                })}
                    </select>
                </div>


                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
)
}


export default ConferenceForm;