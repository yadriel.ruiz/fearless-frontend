import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';



function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
          <Routes>
            <Route index element={<MainPage />} />
            <Route path="conferences/new" element={<ConferenceForm />} />
            <Route path="locations/new" element={<LocationForm />} />

            {/* This is a nested route */}
            <Route path="attendees">
              <Route index={true} element={<AttendeesList attendees={props.attendees} />} />
              <Route path="new" element={<AttendConferenceForm />} />
            </Route>

            <Route path="presentations/" element={<PresentationForm/>} />
          </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;